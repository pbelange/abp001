#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed March 1 11:50:29 2021

@author: pbelange
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import PyNAFF as pnf

     

#==========================================================================================


def generate_coordGrid(xRange,yRange,labels = ['x','y'],nPoints=100):
    '''
    Distribute points uniformly on a 2D grid.
    -----------------------------------------
    Input:
        xRange : range of first coordinate
        yRange : range of second coordinate
        labels : labels to be used in the resulting dataframe
        nPoint : total number of points to generate (sqrt(nPoints) for each coordinate)
    Returns:
        coordinates: dataframe containing the distributed points
    '''

    xVec = np.linspace(xRange[0],xRange[1],int(np.sqrt(nPoints)))
    yVec = np.linspace(yRange[0],yRange[1],int(np.sqrt(nPoints)))
    
    xx,yy = np.meshgrid(xVec,yVec)
    xx,yy = xx.flatten(),yy.flatten()

    return pd.DataFrame(dict(zip(labels,[xx,yy])))




def getTune_pynaff(particle,nterms = 1,skipTurns=0):
    '''
    Compute tunes of a particle using pyNAFF
    ----------------------------------------
    Input:
        particle: pd.Series or dict containing x(t) and y(t) of the particle
        nterms : maximum number of harmonics to search for in the data sample
        skipTurns : number of observations (data points) to skip from the start
    Returns:
        tunes: pd.Series containing the tune in both planes
    '''

    NAFF_X = pnf.naff(np.array(particle['x'])-np.mean(particle['x']), turns=len(particle), nterms=nterms , skipTurns=skipTurns, getFullSpectrum=False)
    NAFF_Y = pnf.naff(np.array(particle['y'])-np.mean(particle['y']), turns=len(particle), nterms=nterms , skipTurns=skipTurns, getFullSpectrum=False)
    
    # TODO: allow more than 1 harmonic (nterms>1)
    # naff returns: [order of harmonic, frequency, Amplitude, Re{Amplitude}, Im{Amplitude] 
    _,Qx,_,Ax_Re,Ax_Im = NAFF_X[0]
    _,Qy,_,Ay_Re,Ay_Im = NAFF_Y[0]    
    
    return pd.Series({'Qx':Qx,'Qy':Qy})


def getTune_fft (particle,showSpectrum=False):
    '''
    Compute tunes of a particle from simple FFT
    -------------------------------------------
    Input:
        particle: pd.Series or dict containing x(t) and y(t) of the particle
        showSpectrum: {True|False} to plot the spectrum used for the fft
    Returns:
        tunes: pd.Series containing the tune in both planes
    '''

    turns = np.arange(1,len(particle['x'])+1)
    freq = np.fft.fftfreq(turns.shape[-1])

    tunes = {'Qx':0,'Qy':0}
    for plane in ['x','y']:
        spectrum = np.fft.fft(particle[plane]-np.mean(particle[plane]))
        idx = np.argmax(np.abs(spectrum))
        tunes['Q'+plane] = freq[idx]


    # For debuging purposes    
    if showSpectrum:
        #plt.figure()
        plt.plot(freq,np.abs(np.fft.fft(particle['x']-np.mean(particle['x']))),label='FFT(x(t))')
        plt.plot(freq,np.abs(np.fft.fft(particle['y']-np.mean(particle['y']))),label='FFT(y(t))')
        plt.xlim([0,np.max(freq)])  
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Amplitude')
        plt.legend()
        #plt.show()

    return pd.Series(tunes)

  

def compute_tunes(tracked,method='pynaff'):
    '''
    Apply the chosen method accross all the particles in a dataframe
    ----------------------------------------------------------------
    Input:
        tracked: pd.DataFrame from madx trackone table
    Returns:
        tuneDF : pd.DataFrame containing the tune of all tracked particles
    '''

    getTune = { 'pynaff':getTune_pynaff,
                'fft'   :getTune_fft     }[method.lower()]
    
    return tracked.groupby('number').apply(getTune)




