import sys
import pandas as pd
import numpy as np


import Backend.Constants as cst





##############################################################
def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return (rho, phi)
##############################################################


##############################################################
def vecNorm(vec):
    return np.sqrt(vec[0]**2 + vec[1]**2)
##############################################################



##############################################################
def normCoordinates(x,px,alpha=None,beta=None,SVD=False):
    
    if SVD:
        alpha,beta = computeAlphaBeta(x,px)
        
    #N0 = [[1/np.sqrt(beta),0],[alpha/np.sqrt(beta), np.sqrt(beta)]]
    x_n  = x/np.sqrt(beta)
    px_n = alpha*x/np.sqrt(beta) + px*np.sqrt(beta)
    
    return x_n,px_n
##############################################################


##############################################################
def computeAlphaBeta(x,px):
    '''Taken from https://arxiv.org/pdf/2006.10661.pdf '''
    
    U,s,V= np.linalg.svd([x,px])         #SVD
    
    N = np.dot(U,np.diag(s))
    theta = np.arctan(-N[0,1]/N[0,0])    #AngleofR(theta)
    co=np.cos(theta) ; si=np.sin(theta)
    
    R = [[co,si],[-si,co]]   
    X = np.dot(N,R)                      #Floquetupto1/det(USR)
    
    beta = np.abs(X[0,0]/X[1,1])
    alpha = X[1,0]/X[1,1]
    
    # dropped
    ex =s[0]*s[1]/(len(x)/2.)            #emit=det(S)/(n/2)
    
    return alpha,beta

##############################################################



##############################################################
def getAction(x,px,alpha=None,beta=None,SVD=False):
    
    if SVD:
        alpha,beta = computeAlphaBeta(x,px)
    gamma = (1+alpha**2)/beta
    
    J = (gamma*x**2  + 2*alpha*x*px + beta*px**2)/2
    
    return J
##############################################################



#############################################################

def generateCoord(Jx,alpha,beta,NParticles,plane='x'):
    
    gamma = (1+alpha**2)/beta
    
    phi = np.linspace(0,2*np.pi,NParticles+1)[1:]
    x  = np.sqrt(2*beta*Jx)*np.cos(phi)
    px = -np.sqrt(2*Jx/beta)*(np.sin(phi)+alpha*np.cos(phi))
    
    coordinates = pd.DataFrame({f'J{plane}':Jx*np.ones(len(x)),f'{plane}':x,f'p{plane}':px})
    
    return coordinates
    

#############################################################