
import sys
import pandas as pd
import numpy as np

from cpymad.madx import Madx
import Backend.Constants as cst
import Backend.WCTools as WCTools
import Backend.MADXTools as MADXTools


def main(toKeep,fileName,original_seq = 'lhc_as-built.seq',sequence = 'lhcb1'):

    # Importing LHC sequences:
    mad = Madx()
    mad.option(echo = False, warn = False)
    mad.call(file=original_seq)


    # Adding beam and extracting twiss table
    mad.command.beam(particle='proton',charge=1,npart=1,energy=7000)
    mad.sequence[sequence].use()
    mad.twiss()
    twiss = mad.table['twiss'].dframe()

    # Editing to remove some elements:
    toKeepDF = twiss.keyword.str.contains('|'.join(toKeep))

    # removing everything except toKeep
    editable = pd.DataFrame({'name':twiss.name.str.split(':',expand=True)[0].copy()})
    editable.reset_index(inplace=True,drop=True)
    editable.insert(0,'mode','remove')
    editable.loc[np.array(toKeepDF),'mode'] = 'skip'

    
    mad.input(MADXTools.seqedit(sequence=sequence,editing=editable, makeThin = False))


    # Saving the new sequence:
    mad.input(f'''SAVE, SEQUENCE={sequence}, 
                        FILE={fileName},
                        NEWNAME={fileName.split('.')[0]};''')