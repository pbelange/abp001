
# Cloning the desired MAD-X distribution
rm -rf MAD-X-TOBIAS
git clone --single-branch --branch wire2021 https://github.com/tpersson/MAD-X.git MAD-X-TOBIAS
mkdir MAD-X-TOBIAS/build
cd MAD-X-TOBIAS/build

cmake .. \
    -DMADX_ONLINE=OFF \
    -DMADX_INSTALL_DOC=OFF \
    -DCMAKE_INSTALL_PREFIX=../dist \
    -DCMAKE_C_FLAGS="-fvisibility=hidden" \
    -DMADX_STATIC=OFF \
    -DBUILD_SHARED_LIBS=OFF
make install

cd ../../

# Make sure the good conda is activated
source /home/pbelange/Apps_V3/miniconda3/bin/activate
export MADXDIR=/home/pbelange/Apps_V3/MAD-X-TOBIAS/dist

set STATIC=0    # if -DMADX_STATIC=ON
set SHARED=0    # if -DBUILD_SHARED_LIBS=ON

# Installing cpymad
rm -rf cpymad
pip uninstall cpymad
git clone https://github.com/hibtc/cpymad.git
cd cpymad
pip install cython wheel
python setup.py build_ext --X11 -lm
pip install -e .
cd ..


echo "Testing Everything:"
which pip
which python
python -c "import cpymad.libmadx as l; l.start()"

