# Validation of the WIRE element in MAD-X


> `cpymad_demo.ipynb` was used to make the plots in the subfolders, but the MAD-X files are made available to reproduce the results

## Problem 1: SOLVED (Wire position inversion (x to -x and y to -y))


## Problem 2: SOLVED (Error on the closed orbit?)


When the physical length of the wire is increased ($`L_{phy}\to 10,000L_w`$), the results are perfectly consistent with the BB wire and the multipolar wire.
```
int_window = 1;
phys_extend = 10000;
wire_tobias    : wire,current := I_w1,L = 0,L_phy := phys_extend*L_w,L_int := int_window*L_w,Xma := r_w*COS(theta_w),Yma := r_w*SIN(theta_w); 
```

<p align="center">
  <img src=./Problem2_output/closed_orbit.png  width="900">
</p>


## Problem 3: SOLVED (Tracking with MAD-X, BBORBIT and ONEPASS)

Same solution as Problem 2.

<p align="center">
  <img src=./Problem3_output/tracking.png  width="900">
</p>


## Convergence of L_phy and L_int

Three observables are compared with the result from a BeamBeam wire:
1. $`\Delta Q_x`$ (tune shift compared to no wire powered)
2. $`\Delta Q_y`$ (tune shift compared to no wire powered)
3. $`\text{Max}(x_{co})`$ (Maximum deviation from the closed orbit)

$`\texttt{L_phy}`$ and $`\texttt{L_int}`$ are increased and the results converge towards the ones of the BeamBeam wire. All the `Rel. Err.` presented are given with regard to the observable obtained with the beambeam wire. 

<p align="center">
  <img src=./Convergence_output/phys_extend.png  width="900">
</p>

<p align="center">
  <img src=./Convergence_output/int_window.png  width="900">
</p>


## Location of wire in embeded drift when L != 0 ? (SOLVED)

Following the last changes, when the $`\texttt{L}`$ parameter of the wire is not 0, that is:
```
wire_tobias    : wire,current := I_w1,L = L_mad,L_phy := L_w,L_int := L_w,Xma := r_w*COS(theta_w),Yma := r_w*SIN(theta_w);
```

the result is equivalent to a wire with $`\texttt{L}=0`$ located in the center of the thick elemen. To study this, the beta function at the focusing quadrupole is compared between the case of a thick wire (`driftTest_log.mad` lines 1-6) and the case of a thin wire offseted by `s_offset` (`driftTest_log.mad` lines 7-end). The results are independant of L_mad and of the current in the wire, but converge differently with different transverse position of the wire r_w. The tune shift from the thick wire is also compared to the tune shift of the thin wire.

<p align="center">
  <img src=./Convergence_output/s_offset_betx.png  width="500">
</p>


<p align="center">
  <img src=./Convergence_output/s_offset_DQx.png  width="500">
</p>

<p align="center">
  <img src=./Convergence_output/s_offset_DQy.png  width="500">
</p>
