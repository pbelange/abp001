option, echo=true, warn=true;

! BBORBIT to allow change of closed orbit
!----------------------------------------
option, bborbit=True;

K_Qf = 6.70501e-03; 
K_Qd =-6.72987e-03;

Qd : quadrupole,l=1.5,k1:=K_Qd;
Qf : quadrupole,l=3.0,k1:=K_Qf;

Energy = 7000;

!==========================================
! WIRE ELEMENT (wire,multipole,beambeam)
!==========================================

! Current and position in polar coordinate
!----------------------------------------
I_w1 = 0;
I_w2 = 0;
I_w3 = 0;

! Common to all wires
L_w = 1.3;
r_w = 8e-3;
theta_w = 0;

!----------------------------------------
int_window = 1;
phys_extend = 10000;
wire_tobias    : wire,current := I_w1,L = 0,L_phy := phys_extend*L_w,L_int := int_window*L_w,Xma := r_w*COS(theta_w),Yma := r_w*SIN(theta_w); 

! Multipole: kicker + multipole since dipolar term of multipole does not contribute to the closed orbit
!----------------------------------------
P0 = 1e9*SQRT(Energy^2-PMASS^2)/CLIGHT;
U0 = 4e-7*PI;
K_multi := -U0*(I_w2*L_w)/(2*PI*P0);

wire_multipole_1of2: multipole,knl := {0,
                    K_multi*1.0/r_w^(2)*COS(-2*theta_w),
                    K_multi*2.0/r_w^(3)*COS(-3*theta_w),
                    K_multi*6.0/r_w^(4)*COS(-4*theta_w),
                    K_multi*24.0/r_w^(5)*COS(-5*theta_w),
                    K_multi*120.0/r_w^(6)*COS(-6*theta_w),
                    K_multi*720.0/r_w^(7)*COS(-7*theta_w),
                    K_multi*5040.0/r_w^(8)*COS(-8*theta_w),
                    K_multi*40320.0/r_w^(9)*COS(-9*theta_w),
                    K_multi*362880.0/r_w^(10)*COS(-10*theta_w),
                    K_multi*3628800.0/r_w^(11)*COS(-11*theta_w),
                    K_multi*39916800.0/r_w^(12)*COS(-12*theta_w),
                    K_multi*479001600.0/r_w^(13)*COS(-13*theta_w),
                    K_multi*6227020800.0/r_w^(14)*COS(-14*theta_w),
                    K_multi*87178291200.0/r_w^(15)*COS(-15*theta_w),
                    K_multi*1307674368000.0/r_w^(16)*COS(-16*theta_w),
                    K_multi*20922789888000.0/r_w^(17)*COS(-17*theta_w),
                    K_multi*355687428096000.0/r_w^(18)*COS(-18*theta_w),
                    K_multi*6402373705728000.0/r_w^(19)*COS(-19*theta_w),
                    K_multi*1.21645100408832e+17/r_w^(20)*COS(-20*theta_w)}, ksl := {0,
                    K_multi*1.0/r_w^(2)*SIN(-2*theta_w),
                    K_multi*2.0/r_w^(3)*SIN(-3*theta_w),
                    K_multi*6.0/r_w^(4)*SIN(-4*theta_w),
                    K_multi*24.0/r_w^(5)*SIN(-5*theta_w),
                    K_multi*120.0/r_w^(6)*SIN(-6*theta_w),
                    K_multi*720.0/r_w^(7)*SIN(-7*theta_w),
                    K_multi*5040.0/r_w^(8)*SIN(-8*theta_w),
                    K_multi*40320.0/r_w^(9)*SIN(-9*theta_w),
                    K_multi*362880.0/r_w^(10)*SIN(-10*theta_w),
                    K_multi*3628800.0/r_w^(11)*SIN(-11*theta_w),
                    K_multi*39916800.0/r_w^(12)*SIN(-12*theta_w),
                    K_multi*479001600.0/r_w^(13)*SIN(-13*theta_w),
                    K_multi*6227020800.0/r_w^(14)*SIN(-14*theta_w),
                    K_multi*87178291200.0/r_w^(15)*SIN(-15*theta_w),
                    K_multi*1307674368000.0/r_w^(16)*SIN(-16*theta_w),
                    K_multi*20922789888000.0/r_w^(17)*SIN(-17*theta_w),
                    K_multi*355687428096000.0/r_w^(18)*SIN(-18*theta_w),
                    K_multi*6402373705728000.0/r_w^(19)*SIN(-19*theta_w),
                    K_multi*1.21645100408832e+17/r_w^(20)*SIN(-20*theta_w)};

wire_multipole_2of2: kicker, L=0, HKICK := -K_multi*1.0/r_w^(1)*COS(-1*theta_w), VKICK := K_multi*1.0/r_w^(1)*SIN(-1*theta_w);



! Beam-beam element
!----------------------------------------
LHC_C = 26659;
gamma_r = Energy/PMASS + 1;
beta_r = SQRT(1-1/gamma_r^2);
N_p := I_w3*LHC_C/(QELECT*CLIGHT)/((1+beta_r^2)/beta_r);

! Effective bb charge (counter-rotating beam -> twice bb interactions over a length L_w)
effCharge := 2*N_p/LHC_C*L_w;

! charge is set negative to attract for positive current and repulse for positive current
wire_bb   : beambeam, charge := -effCharge, Xma := r_w*COS(theta_w),Yma := r_w*SIN(theta_w),sigx = 1e-6, sigy = 1e-6,width=1,BBDIR = -1;



!==========================================
! Defining sequence
!==========================================

FODO:sequence, refer = center, L=100;
!------------------------
Q1  : Qd                 , at = 0.75;
Q2  : Qf                 , at = 50;
W1  : wire_tobias        , at = 55;
W2_1: wire_multipole_1of2, at = 55;
W2_2: wire_multipole_2of2, at = 55;
W3  : wire_bb            , at = 55;
Q3  : Qd                 , at = 99.25;
!------------------------
endsequence;


!==========================================
! Defining Beam 
!==========================================
beam,   particle = proton,
        charge   = 1,
        npart    = 1,
        energy   = Energy;

use, sequence = FODO; 


!==========================================
! TWISS
!==========================================


! Step 1
twiss,table=twiss_ref,file="Problem2_output/twiss_ref.tfs";

! Step 2
I_w1 = 230;
I_w2 = 0;
I_w3 = 0;
twiss,table=twiss_wire_tobias,file="Problem2_output/twiss_wire_tobias.tfs";

! Step 3
I_w1 = 0;
I_w2 = 230;
I_w3 = 0;
twiss,table=twiss_wire_multipole,file="Problem2_output/twiss_wire_multipole.tfs";

! Step4
I_w1 = 0;
I_w2 = 0;
I_w3 = 230;
twiss,table=twiss_wire_bb,file="Problem2_output/twiss_wire_bb.tfs";
