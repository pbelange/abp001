import numpy as np
import pandas as pd

from cpymad.madx import Madx

import xline as xl
import xobjects as xo
import xtrack as xt
import xpart as xp



# Importing LHC sequences:
mad = Madx()
mad.option(echo = True, warn = True)
mad.call(file='/afs/cern.ch/eng/lhc/optics/lhc_current/lhc_as-built.seq')

# Applying optics
mad.call(file='/afs/cern.ch/eng/lhc/optics/lhc_current/opt_400_10000_400_3000.madx')

# Adding beam 
mad.command.beam(particle='proton',charge=1,npart=1,energy=7000)

    
# Sending sequence to xsuite
mad.input(f'''use, sequence = lhcb1; makethin,sequence = lhcb1;''')
xsequence = xl.Line.from_madx_sequence(mad.sequence['lhcb1'])

# For local computation:
context = xo.ContextCpu()

# Setting up the tracker
tracker = xt.Tracker(_context=context,sequence=xsequence)


n_part=10
particles = xt.Particles(_context=context,
                    p0c=6500e9,
                    x=np.random.uniform(-1e-3, 1e-3, n_part),
                    px=np.random.uniform(-1e-5, 1e-5, n_part),
                    y=np.random.uniform(-2e-3, 2e-3, n_part),
                    py=np.random.uniform(-3e-5, 3e-5, n_part),
                    zeta=np.random.uniform(-1e-2, 1e-2, n_part),
                    delta=np.random.uniform(-1e-4, 1e-4, n_part),
                    )

## Track (saving turn-by-turn data)
n_turns = 10
tracker.track(particles, num_turns=n_turns,turn_by_turn_monitor=True)
    
print(tracker.record_last_track.x)
    